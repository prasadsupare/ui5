sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/BusyIndicator",
	"sap/m/MessageToast",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV",
	"sap/m/MessageBox",
	"sap/ui/model/Sorter",
	"sap/ui/table/library",
	"sap/ui/thirdparty/jquery",
	"sap/ui/table/RowAction",
	"sap/ui/table/RowActionItem",
	"sap/ui/table/RowSettings",
	"sap/ui/core/Fragment"

], function(Controller, JSONModel, Filter, FilterOperator, BusyIndicator, MessageToast, Export, ExportTypeCSV, MessageBox, Sorter,
	library, jQuery, RowAction, RowActionItem, RowSettings, Fragment) {
	"use strict";
	var oView, oComponent, oController;
	var SortOrder = library.SortOrder;
	var ListofVendor = [];
	return Controller.extend("com.vSimpleApp.controller.ShowTiles", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.vSimpleApp.view.view.ShowTiles
		 */
		onInit: function() {
			
			oController = this;
			oView = this.getView();
			oComponent = this.getOwnerComponent();
			this.oSF = oView.byId("searchField");
			var sRootPath = jQuery.sap.getModulePath("com.vSimpleApp");

			//get the json data model.
			var oModel = new JSONModel([sRootPath, "data/LandingPageData.json"].join("/"));
			this.getView().setModel(oModel, "LandingPageModel");

			var manifests = new sap.ui.model.json.JSONModel();
			manifests.loadData("model/cardManifests.json");
			this.getView().setModel(manifests, "manifests");
			console.log(manifests);

			var CountModel = new JSONModel();
			oView.setModel(CountModel, "CountModel");

			var jsonData = new sap.ui.model.json.JSONModel();
			jsonData.loadData("data/Data.json");
			this.getView().setModel(jsonData, "jsonData");
			console.log(jsonData);
			var inputModel = new JSONModel({
				expression: ""
			});
			this.getView().setModel(inputModel, "inputModel");

			this.getVendorList();
			this.getPurchaseOrderList();

			this.getVendorCountListByPO();
			this.getServiceHeader();
			this.getTopFiveVendorsNames();
				this.getTopProductsFirst();
			// set explored app's demo model on this sample

			oView.setModel(new JSONModel({
				globalFilter: "",
				availabilityFilterOn: false,
				cellFilterOn: false
			}), "ui");
			this._oGlobalFilter = null;
			this._oPriceFilter = null;

			var fnPress = this.handleActionPress.bind(this);

			this.modes = [{
					key: "Navigation",
					text: "Navigation",
					handler: function() {
						var oTemplate = new RowAction({
							items: [
								new RowActionItem({
									type: "Navigation",
									press: fnPress,
									visible: "{Available}"
								})
							]
						});
						return [1, oTemplate];
					}
				}

			];

			this.getView().setModel(new JSONModel({
				items: this.modes
			}), "modes");
			this.switchState("Navigation");
			
			var fjson = new sap.ui.model.json.JSONModel();
			var fjson2 = new sap.ui.model.json.JSONModel();
			var fjson3 = new sap.ui.model.json.JSONModel();
			$.ajax({                                      
			      url: 'http://localhost:80/tableData.php?rule=1',                  
			      async:false,        
			      success: function(data)          //on recieve of reply
			      {
					fjson.setData(JSON.parse(data));
			      },
			      error: function(err)
			      {
			    	  console.log(err);
			      }
			    });
			    
			    $.ajax({                                      
			      url: 'http://localhost:80/tableData.php?rule=2',                  
			      async:false,        
			      success: function(data)          //on recieve of reply
			      {
					fjson2.setData(JSON.parse(data));
			      },
			      error: function(err)
			      {
			    	  console.log(err);
			      }
			    });
			    
			    $.ajax({                                      
			      url: 'http://localhost:80/tableData.php?rule=5',                  
			      async:false,        
			      success: function(data)          //on recieve of reply
			      {
					fjson3.setData(JSON.parse(data));
			      },
			      error: function(err)
			      {
			    	  console.log(err);
			      }
			    });
			    
			//console.log(this.byId("tablePoorders").getColumns());
			var clen = this.byId("tablePoorders").getColumns().length;
			if(fjson2.oData.result[0].enabled=="no")
			{
				this.byId("top5v").setVisible(false);
			}
			
			if(fjson2.oData.result[1].enabled=="no")
			{
				this.byId("top5vv").setVisible(false);
			}
			
			if(fjson2.oData.result[2].enabled=="no")
			{
				this.byId("top5p").setVisible(false);
			}
			
			for(var i=0;i<clen;i++)
			{
				
				if(fjson.oData.result[i].enabled=="no")
					{
						this.byId("tablePoorders").getColumns()[i].setVisible(false);
					}
					
			}
			
			if(fjson3.oData.result[0].enabled=="no")
			{
			this.byId("tcard").setVisible(false);
			}

		},

		switchState: function(sKey) {
			var oTable = this.byId("tablePoorders");
			var iCount = 0;
			var oTemplate = oTable.getRowActionTemplate();
			if (oTemplate) {
				oTemplate.destroy();
				oTemplate = null;
			}

			for (var i = 0; i < this.modes.length; i++) {
				if (sKey == this.modes[i].key) {
					var aRes = this.modes[i].handler();
					iCount = aRes[0];
					oTemplate = aRes[1];
					break;
				}
			}

			oTable.setRowActionTemplate(oTemplate);
			oTable.setRowActionCount(iCount);

		},

		handleActionPress: function(oEvent) {
			var oLookupModel = this.getOwnerComponent().getModel("Lookup");

			var PurchaseNumber = oEvent.getParameter("row").getRowBindingContext().getProperty("Ebeln");

			/*	MessageToast.show("Item " + (oItem.getText() || oItem.getType()) + " pressed for Po with id " +
					oEvent.getParameter("row").getRowBindingContext().getProperty("Ebeln"));*/

			try {
				//	var PurchaseNumber = oEvent.getSource().data("Ebeln");
				oComponent.getRouter().navTo("PurchaseItemDetails", {
					PoNumber: PurchaseNumber
				});
			} catch (ex) {
				MessageBox.error(ex);
			}

		},

		NavigatePurchaseItemDetails: function(oEvent) {

			try {
				var PurchaseNumber = oEvent.getSource().data("Ebeln");
				oComponent.getRouter().navTo("PurchaseItemDetails", {
					PoNumber: PurchaseNumber
				});
			} catch (ex) {
				MessageBox.error(ex);
			}

			//oComponent.getRouter().navTo("Dashboard2");
		},
		pressGenericTile: function(evt) {
			//navigate the property is selected subheader.
			if (evt.getSource().getProperty("header") === "Vendor Master") {
				oComponent.getRouter().navTo("VendorCreate");
			} else if (evt.getSource().getProperty("header") === "Purchase Order") {
				oComponent.getRouter().navTo("PurchaseOrderTable");
			} else if (evt.getSource().getProperty("header") === "Post Goods Receipt") {
				oComponent.getRouter().navTo("GoodReceipt");
			} else if (evt.getSource().getProperty("header") === "Book Vendor Invoice") {
				oComponent.getRouter().navTo("Dashboard");
			} else if (evt.getSource().getProperty("header") === "Vendor Rebate Management") {
				oComponent.getRouter().navTo("DashboardVendor");
			}

		},

		tile: function(oEvent) {
			oComponent.getRouter().navTo("TileDashboard");
		},
		// Get  Top five vendor and get their counts
		getVendorCountListByPO: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//BusyIndicator.show(0);
			return new Promise(function (resolve1, reject1) { 
			oModel.read("/POHeaderSet", {
				//	oModel.read("/POItemSet",{
				success: function(oData) {
					//		console.log(oData);
					var item = oData.results.length;
					var ListofVendoritem = [];

					var ListofVendoritemTwo = [];
					for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
						//		console.log(iRowIndex);
						var Lifnrr = oData.results[iRowIndex].Lifnr;

						ListofVendoritem.push({
							Lifnr: Lifnrr

						});
					}

					//			console.log(ListofVendoritem);

					var index = {};
					var result = [];

					ListofVendoritem.forEach(function(point) {
						var key = "" + point.Lifnr + " "

						if (key in index) {
							index[key].count++;
						} else {
							var newEntry = {
								Lifnr: point.Lifnr,
								//	Name1 : vendorname,
								count: 1
							};
							index[key] = newEntry;
							result.push(newEntry);

						}
					});
					//	console.log(result);

					result.sort(function(a, b) {
						return b.count - a.count;
					});
					//		console.log(result);
					var resultlengrh = result.length;
					var ListofVendoritemTwo = [];
					for (var iRowIndex = 1; iRowIndex <= 5; iRowIndex++) {

						var Lifnrr = result[iRowIndex].Lifnr;
						var counts = result[iRowIndex].count;
						var name = result[iRowIndex].VendorNameee;
						if (Lifnrr !== "" || Lifnrr !== undefined) {
							for(var x = 0; x < ListofVendor.length; x++)
							{
								if(Lifnrr==ListofVendor[x].Lifnr)
								{
									var vendorname = ListofVendor[x].Name1;
									console.log(vendorname);
									name = vendorname;
								}
							}
						}
						ListofVendoritemTwo.push({
							Lifnr: Lifnrr,
							count: counts,
							name : name

						});
					}

					console.log(ListofVendoritemTwo);

					//top five vendor model with the count
					var CountModel = oView.getModel("CountModel");
					CountModel.setData(ListofVendoritemTwo);
					//	oView.setModel(CountModel, "CountModel");
					console.log(CountModel);

					//show popover in chart 
					/*	 var oPopOverBar = oView.byId("idPopOverBar");
            oPopOverBar.connect(CountModel.getVizUid());*/

					var itemsc = CountModel.oData.length;
					for (var iRowIndex = 0; iRowIndex < itemsc; iRowIndex++) {
						var Lifnrr = CountModel.oData[iRowIndex].Lifnr;
						var aFilter = [
							new sap.ui.model.Filter({
								path: "Lifnr",
								operator: sap.ui.model.FilterOperator.EQ,
								value1: Lifnrr
							})

						];
						/*		oModel.read("/Fetch_Vendor_DetailsSet", {
									filters: aFilter,
									success: function(suc) {
										console.log(suc);
										var item = suc.results.length;
												var VendorssList = [];
												var itemsc = CountModel.oData.length;
											for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
												var i=0 
									//		for (; i<itemsc ; i++ ){
											var lifnr = suc.results[iRowIndex].Lifnr;
											var vendorname = suc.results[iRowIndex].Name1;
											
															
															while (i < itemsc) {
																	if (Lifnrr == lifnr) {
													 	VendorssList.push({
																	Lifnr: lifnr,
																	Name1: vendorname
									
																});
															
																	}
																	  i++;
															}
											
											
										
											console.log(vendorname);

										
									}
										console.log(VendorssList);
									},
									error: function(err) {
										console.log(err);
									}
								});*/
					}

					var ListofVendorTopThree = [];

					for (var iRowIndex = 1; iRowIndex <= 3; iRowIndex++) {
						//		console.log(iRowIndex);
						var Lifnrr = result[iRowIndex].Lifnr;
						var Count = result[iRowIndex].count;
						ListofVendorTopThree.push({
							Lifnr: Lifnrr,
							count: Count

						});
					}

					console.log(ListofVendorTopThree);
					var ListofVendorTopThreeModel = new JSONModel();
					ListofVendorTopThreeModel.setData(ListofVendorTopThree);
					oView.setModel(ListofVendorTopThreeModel, "ListofVendorTopThreeModel");
					console.log(ListofVendorTopThreeModel);
				},
				error: function(oError) {
					//BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		});
		},
		getTopFiveVendorsNames: function() {
			var oModel = this.getOwnerComponent().getModel("VHeader");
			var CountModels = oView.getModel("CountModel");
			console.log(CountModels);
			var itemsc = CountModels.oData;
			var VendorssList = [];
			//	var Lifnrr = 1005;
			for (var iRowIndex = 0; iRowIndex < itemsc; iRowIndex++) {
				var Lifnrr = CountModels.oData[iRowIndex].Lifnr;
				var aFilter = [
					new sap.ui.model.Filter({
						path: "Lifnr",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: Lifnrr
					})

				];
				oModel.read("/Fetch_Vendor_DetailsSet", {
					filters: aFilter,
					success: function(suc) {
						console.log(suc);
						var item = suc.results.length;

						for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {

							var lifnr = suc.results[iRowIndex].Lifnr;

							var vendorname = suc.results[iRowIndex].Name1;
							if (Lifnrr == lifnr) {
								oView.getModel("CountModel").setProperty("Vendorname", vendorname); // setData(oData.results);

								VendorssList.push({
									Lifnr: lifnr,
									Name1: vendorname

								});
							}
							console.log(vendorname);

						}

						console.log(VendorssList);
					},
					error: function(err) {
						console.log(err);
					}
				});
			}
		},

		getVendorList: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//BusyIndicator.show(0);
			oModel.read("/Fetch_Vendor_DetailsSet", {
				success: function(oData) {
					var item = oData.results.length;
					

					for (var iRowIndex = 0; iRowIndex <= 2600; iRowIndex++) {
						var odata = oData.results[iRowIndex];
						if(odata!==undefined)
						{
							var Lifnrr = odata.Lifnr;
							var Name1r = odata.Name1;
							ListofVendor.push({
								Lifnr: Lifnrr,
								Name1: Name1r
							});
						}

					}
					console.log(ListofVendor);

					var Count = new sap.ui.model.json.JSONModel({
						item: item

					});
					oView.setModel(Count, "Count");

					//BusyIndicator.hide();
					var oLookupModel = that.getOwnerComponent().getModel("Lookup");
					oLookupModel.setProperty("/DisplyaVendorList", ListofVendor);
					oLookupModel.refresh(true);
					//that.getMaterialList();
				},
				error: function(oError) {
					//BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},

		getPurchaseOrderList: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//	BusyIndicator.show(0);
			//	oModel.read("/POListSet", {
			oModel.read("/just_poheaderSet", {
				success: function(oData) {

					BusyIndicator.hide();
					var itemPO = oData.results.length;
					var ListofPurchaseOrder = [];

					for (var iRowIndex = 1300; iRowIndex <= 1313; iRowIndex++) {
						var odataset = oData.results[iRowIndex];

						var Compcode = odataset.Bukrs;
						var polost = odataset.Ebeln;
						var pogrp = odataset.Ekgrp;
						var poorg = odataset.Ekorg;
						var lifnrr = odataset.Lifnr;
						var currency = odataset.Waers;
						var createddate = odataset.Bedat;
						var Createdby = odataset.Ernam;

						ListofPurchaseOrder.push({
							Bukrs: Compcode,
							Ebeln: polost,
							Ekgrp: pogrp,
							Ekorg: poorg,
							Lifnr: lifnrr,
							Waers: currency,
							Bedat: createddate,
							Ernam: Createdby

						});

					}
					//	console.log(ListofPurchaseOrder);

					var CountPo = new sap.ui.model.json.JSONModel({
						item: itemPO

					});
					oView.setModel(CountPo, "CountPo");

					//	console.log(oData);

					var oLookupModel = that.getOwnerComponent().getModel("Lookup");
					oLookupModel.setProperty("/POOrderList", ListofPurchaseOrder);
					oLookupModel.refresh(true);
					//that.getMaterialList();
				},
				error: function(oError) {
					BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},
		getTopProductsSecond: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//BusyIndicator.show(0);
			oModel.read("/POHeaderSet", {
				//	oModel.read("/POItemSet",{
				success: function(oData) {
					//		console.log(oData);
					var item = oData.results.length;
					var ListofVendoritem = [];

					var ListofVendoritemTwo = [];
					for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
						//		console.log(iRowIndex);
						var Lifnrr = oData.results[iRowIndex].Lifnr;

						ListofVendoritem.push({
							Lifnr: Lifnrr

						});
					}

					//			console.log(ListofVendoritem);

					var index = {};
					var result = [];

					ListofVendoritem.forEach(function(point) {
						var key = "" + point.Lifnr + " "

						if (key in index) {
							index[key].count++;
						} else {
							var newEntry = {
								Lifnr: point.Lifnr,
								//	Name1 : vendorname,
								count: 1
							};
							index[key] = newEntry;
							result.push(newEntry);

						}
					});
					//	console.log(result);

					result.sort(function(a, b) {
						return b.count - a.count;
					});
					//		console.log(result);
					var resultlengrh = result.length;
					var ListofVendoritemTwo = [];
					for (var iRowIndex = 1; iRowIndex <= 5; iRowIndex++) {

						var Lifnrr = result[iRowIndex].Lifnr;
						var counts = result[iRowIndex].count;

						ListofVendoritemTwo.push({
							Lifnr: Lifnrr,
							count: counts

						});
					}

					console.log(ListofVendoritemTwo);

					var CountModel = oView.getModel("CountModel");
					CountModel.setData(ListofVendoritemTwo);
					//	oView.setModel(CountModel, "CountModel");
					console.log(CountModel);

					//show popover in chart 
					/*	 var oPopOverBar = oView.byId("idPopOverBar");
            oPopOverBar.connect(CountModel.getVizUid());*/

					var itemsc = CountModel.oData.length;
					for (var iRowIndex = 0; iRowIndex < itemsc; iRowIndex++) {
						var Lifnrr = CountModel.oData[iRowIndex].Lifnr;
						var aFilter = [
							new sap.ui.model.Filter({
								path: "Lifnr",
								operator: sap.ui.model.FilterOperator.EQ,
								value1: Lifnrr
							})

						];
						/*		oModel.read("/Fetch_Vendor_DetailsSet", {
									filters: aFilter,
									success: function(suc) {
										console.log(suc);
										var item = suc.results.length;
												var VendorssList = [];
												var itemsc = CountModel.oData.length;
											for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
												var i=0 
									//		for (; i<itemsc ; i++ ){
											var lifnr = suc.results[iRowIndex].Lifnr;
											var vendorname = suc.results[iRowIndex].Name1;
											
															
															while (i < itemsc) {
																	if (Lifnrr == lifnr) {
													 	VendorssList.push({
																	Lifnr: lifnr,
																	Name1: vendorname
									
																});
															
																	}
																	  i++;
															}
											
											
										
											console.log(vendorname);

										
									}
										console.log(VendorssList);
									},
									error: function(err) {
										console.log(err);
									}
								});*/
					}

					var ListofVendorTopThree = [];

					for (var iRowIndex = 1; iRowIndex <= 3; iRowIndex++) {
						//		console.log(iRowIndex);
						var Lifnrr = result[iRowIndex].Lifnr;
						var Count = result[iRowIndex].count;
						ListofVendorTopThree.push({
							Lifnr: Lifnrr,
							count: Count

						});
					}

					console.log(ListofVendorTopThree);
					var ListofVendorTopThreeModel = new JSONModel();
					ListofVendorTopThreeModel.setData(ListofVendorTopThree);
					oView.setModel(ListofVendorTopThreeModel, "ListofVendorTopThreeModel");
					console.log(ListofVendorTopThreeModel);
				},
				error: function(oError) {
					//BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},
		getTopProductsFirst: function() {
			var oModel1 = this.getOwnerComponent().getModel("VHeader");
			oModel1.read("/Max_MaterialSet", {

							//	oModel.read("/PO_DetailsSet()", {
							//filters: aFilter,
							success: function(odata) {
								console.log(odata);
								var arr = [];
								arr = odata.results;
								console.log(arr);
								

								function foo(arr) {
								  var a = [],
								    b = [],
								    prev;
								  arr.sort();
								  for (var i = 0; i < arr.length; i++) {
								    if (arr[i].Txz01 !== prev) {
								      a.push(arr[i].Txz01);
								      b.push(1);
								    } else {
								      b[b.length - 1]++;
								    }
								    prev = arr[i].Txz01;
								  }
								
								  return [a, b];
								}
								
								var result = foo(arr);
								//console.log('[' + result[0] + ']','[' + result[1] + ']')
								var final = [];
								for(var z = 0 ; z < result[1].length ; z++)
								{
									if(result[1][z]!==1)
									{
										//result[0][z].pop();
										//result[1][z].pop();
									final.push([result[0][z],result[1][z]]);
									//final[1].push(result[1][z]);
									}
								}
								//console.log(final);
								function compareSecondColumn(a, b) {
								    if (a[1] === b[1]) {
								        return 0;
								    }
								    else {
								        return (a[1] > b[1]) ? -1 : 1;
								    }
								}
								final.sort(compareSecondColumn);
								console.log(final);
								var top5products = [];
								for(var zz = 0; zz <= 4; zz++)
								{
									top5products.push({
										prod: final[zz][0],
										count: final[zz][1]
									});
								}
								console.log(top5products);
								var top5productsModel = new JSONModel();
								top5productsModel.setData(top5products);
								oView.setModel(top5productsModel, "top5products");
								console.log(top5productsModel);
							},
							error: function(er) {
								console.log(er);
							}
						});
		/*	var that = this;
			var odataset;
			var oModel1 = this.getOwnerComponent().getModel("VHeaderM");
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//	BusyIndicator.show(0);
			oModel1.read("/POListSet", {
				success: function(oData) {
					console.log(oData);
					BusyIndicator.hide();
					var itemPO = oData.results.length;
					var ListofPurchaseOrder = [];

					for (var iRowIndex = 0; iRowIndex <= itemPO; iRowIndex++) {
						odataset = oData.results[iRowIndex].Ebeln;
					
						oModel1.read("/PO_DetailsSet(Purchaseorder eq '" + odataset + "')", {

							//	oModel.read("/PO_DetailsSet()", {
							//filters: aFilter,
							success: function(odata) {
								console.log(odata);
								var Materialno = odata.Bukrs;
								ListofPurchaseOrder.push({
									Material: Materialno

								});
							},
							error: function(er) {
								console.log(er);
							}
						});

						//	}

					}
					console.log(ListofPurchaseOrder);

					var CountPo = new sap.ui.model.json.JSONModel({
						item: itemPO

					});
					oView.setModel(CountPo, "CountPo");

					//	console.log(oData);

				},
				error: function(oError) {
					BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});

			var aFilter = [
				new sap.ui.model.Filter({
					path: "Purchaseorder",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: odataset
				})

			];
			oModel.read("/PO_DetailsSet(Purchaseorder eq '" + odataset + "')", {

				//	oModel.read("/PO_DetailsSet()", {
				//filters: aFilter,
				success: function(odata) {
					console.log(odata);
					var Materialno = odata.Bukrs;
					ListofPurchaseOrder.push({

						Material: Materialno

					});
				},
				error: function(er) {
					console.log(er);
				}
			});*/

		},
		getServiceHeader: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//	BusyIndicator.show(0);
			var po = "4500004837";
			
			
			// Mi ithe po chi value hard code ghetli ahe, but tula ithe loop firvaych ahe.  count, material and disription eka model made print kar.
			// baki mi bind karte. console la print kar
			//count cha code below mension kela ahe.. 
			/*	oModel.read("/PO_DetailsSet(Purchaseorder eq '" + odataset + "')", {}

				//	oModel.read("/PO_DetailsSet()", {}
		*/
			
			
			var aFilter = [
				new sap.ui.model.Filter({
					path: "Purchaseorder",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: po
				})

			];
			oModel.read("/PO_DetailsSet", {
				filters: aFilter,
				success: function(oData) {
					console.log(oData);
					
						var resultlengrh = 	oData.results.length;
					var MaterialListForCurrentPo = [];
					for (var iRowIndex = 0; iRowIndex <= resultlengrh; iRowIndex++) {
						if(oData.results[iRowIndex]!==undefined)
						{
							var materno=	oData.results[iRowIndex].Material;
							var matdis = oData.results[iRowIndex].ShortText;
							MaterialListForCurrentPo.push({
								Material: materno,
								ShortText: matdis
							});
						}
			
					}

					console.log(MaterialListForCurrentPo);
					
					
						var index = {};
					var result = [];

					MaterialListForCurrentPo.forEach(function(point) {
						var key = "" + point.Material + " "

						if (key in index) {
							index[key].count++;
						} else {
							var newEntry = {
								Material: point.Material,
								//	Name1 : vendorname,
								count: 1
							};
							index[key] = newEntry;
							result.push(newEntry);

						}
					});
					console.log(result);

					result.sort(function(a, b) {
						return b.count - a.count;
					});
					//		console.log(result);
					var resultlength = result.length;
					var MatListWithCount = [];
					for (var iRowIndex = 0; iRowIndex <= resultlength; iRowIndex++) {
						if(result[iRowIndex]!==undefined)
						{
								var Material = result[iRowIndex].Material;
								var counts = result[iRowIndex].count;
								var MatDis =  result[iRowIndex].ShortText;
								MatListWithCount.push({
									Material: Material,
									ShortText: MatDis,
									count: counts
		
								});
						}
					
					}

					console.log(MatListWithCount);
					

				},

				error: function(oError) {
					BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},
		_filter: function() {
			var oFilter = null;

			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}

			this.byId("tablePoorders").getBinding().filter(oFilter, "Application");
		},
		clearAllFiltersPOtable: function(oEvent) {
			var oTable = oView.byId("tablePoorders");

			var oUiModel = oView.getModel("ui");
			oUiModel.setProperty("/globalFilter", "");
			oUiModel.setProperty("/availabilityFilterOn", false);

			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			this._filter();

			var aColumns = oTable.getColumns();
			for (var i = 0; i < aColumns.length; i++) {
				oTable.filter(aColumns[i], null);
			}

			oTable.getBinding().sort(null);
			this._resetPOTableSortingState();
		},
		_resetPOTableSortingState: function() {
			var oTable = this.byId("tablePoorders");
			var aColumns = oTable.getColumns();
			for (var i = 0; i < aColumns.length; i++) {
				aColumns[i].setSorted(false);
			}
		},
		onSearchEbeln: function(oEvent) {

			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getSource().getValue();
			if (sQuery) {
				aFilter.push(
					new Filter("Ebeln", FilterOperator.EQ, sQuery));

			}
			// update list binding
			var list = this.getView().byId("awaitingTable");
			var binding = list.getBinding("items");
			binding.filter(aFilter, "Application");

		},
		onSortTable: function() {
			var oView = this.getView(),
				aStates = [undefined, "asc", "desc"],
				aStateTextIds = ["sortNone", "sortAscending", "sortDescending"],
				sMessage,
				//	iOrder = oView.getModel("appView").getProperty("/order");
				iOrder = this.getOwnerComponent().getModel("Lookup").getProperty("/POOrderList");

			// Cycle between the states
			iOrder = (iOrder + 1) % aStates.length;
			var sOrder = aStates[iOrder];

			oView.getModel("Lookup").setProperty("/POOrderList", iOrder);
			oView.byId("awaitingTable").getBinding("items").sort(sOrder && new Sorter("Waers", sOrder === "desc"));

		},
		onDataExport: function(oEvent) {
			var oLookupModel = this.getOwnerComponent().getModel("Lookup");
			console.log(oLookupModel);
			var data = oLookupModel.oData.POOrderList;
			var localdata = new JSONModel();
			localdata.setData(data);
			oView.setModel(localdata, "localdata");
			console.log(localdata);
			jQuery.sap.require("sap.ui.core.util.Export");
			jQuery.sap.require("sap.ui.core.util.ExportTypeCSV");
			var oExport = new Export({

				// Type that will be used to generate the content. Own ExportType's can be created to support other formats
				exportType: new ExportTypeCSV({
					fileExtension: "csv",
					separatorChar: ";"
				}),

				// Pass in the model created above
				models: localdata,
				//this.getView().getModel("Lookup"),

				// binding information for the rows aggregation
				rows: {
					path: "/"
				},

				// column definitions with column name and binding info for the content

				columns: [{
					name: "Company Code",
					template: {
						content: "{Bukrs}"
					}
				}, {
					name: "Purchase Order",
					template: {
						content: "{Ebeln}"
					}
				}, {
					name: "Purchase Group",
					template: {
						content: "{Ekgrp}"
					}
				}, {
					name: "Purchase Org",
					template: {
						content: "{Ekorg}"

					}
				}, {
					name: "Currency",
					template: {
						content: "{Waers}"
					}
				}]
			});

			// download exported file
			oExport.saveFile().catch(function(oError) {
				MessageBox.error("Error when downloading data. Browser might not be supported!\n\n" + oError);
			}).then(function() {
				oExport.destroy();
			});
		},

		TableFullScreen: function() {

			this._oDialoga = this.getView().byId("hellodailog");
			if (!this._oDialoga) {
				this._oDialoga = sap.ui.xmlfragment("com.vSimpleApp.view.fullscreenview", this);
				//this.getView().addDependent(pressDialogcal);
				//  this.pressDialogcal.setModel(this.getView().getModel());
				this._oDialoga.open();
			}

		},

		CloseFullScreenTable: function() {
			this._oDialoga.close();
			this._oDialoga.destroy();
		},
		onExitTableFullScreen: function() {
			if (this._oDialoga) {
				this._oDialoga.destroy();
			}
		},

		onSearchFilterData: function() {
			this.pressDialogFilter = this.getView().byId("ListDialog");
			if (!this.pressDialogFilter) {
				this.pressDialogFilter = sap.ui.xmlfragment("com.vSimpleApp.view.fragment.FilterFile", this);
				//this.getView().addDependent(pressDialogFilter);
				//  this.pressDialogFilter.setModel(this.getView().getModel());
				this.pressDialogFilter.open();
			}
		},

		onCloseFu1: function() {
			this.pressDialogFilter.close();
			this.pressDialogFilter.destroy();
		},
		onExit1: function() {
			if (this.pressDialogFilter) {
				this.pressDialogFilter.destroy();
			}
		},
		onPressCalculator: function() {
			this.pressDialogcal = this.getView().byId("ListDialog");
			if (!this.pressDialogcal) {
				this.pressDialogcal = sap.ui.xmlfragment("com.vSimpleApp.view.FileCalculator", this);
				//this.getView().addDependent(pressDialogcal);
				//  this.pressDialogcal.setModel(this.getView().getModel());
				this.pressDialogcal.open();
			}
		},

		onCloseFu: function() {
			this.pressDialogcal.close();
			this.pressDialogcal.destroy();
		},
		onExit: function() {
			if (this.pressDialogcal) {
				this.pressDialogcal.destroy();
			}
		},

		clearInput: function() {
			this.getView().getModel("inputModel").setProperty("/expression", "");
		},
		editInput: function(ch) {
			var expression = this.getView().getModel("inputModel").getProperty("/expression");

			//	oView.byId("inputboxid").setText(expression);
			if ("+/*-".indexOf(ch) == -1) {
				expression += ch;
			} else {
				expression += (" " + ch + " ");
			}
			this.getView().getModel("inputModel").setProperty("/expression", expression);
		},

		validCompute: function() {
				var expression = this.getView().getModel("inputModel").getProperty("/expression");
				var expList = expression.split(" ");
				if (isValid(expList) == true && expList.length) {
					var postFix = changeToPostfix(expList);
					var result = evaluatePostFix(postFix);
					if (result != null) {
						this.getView().getModel("inputModel").setProperty("/expression", result);
						return;
					}
				}
				alert("Incomplete or Invalid Expression. Please review the expression.");

			}
			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf com.vSimpleApp.view.view.ShowTiles
			 */
			//	onBeforeRendering: function() {
			//
			//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.vSimpleApp.view.view.ShowTiles
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.vSimpleApp.view.view.ShowTiles
		 */
		//	onExit: function() {
		//
		//	}

	});

});
var isValid = function(expList) {
	for (var i = 1; i < expList.length; ++i) {
		if (("+-/*".indexOf(expList[i - 1]) !== -1) && ("+-/*".indexOf(expList[i]) !== -1))
			return false;
	}
	console.log(expList[expList.length - 1]);
	return ("+-/*".indexOf(expList[expList.length - 1]) === -1);
}
var changeToPostfix = function(expression) {

	var postFix = new Array(); // To store the resultant postFix expression
	var stack = new Array(); // To Store the operators
	var precedence = { //Defing the precedence rule
		"+": 1,
		"-": 1,
		"/": 2,
		"*": 2
	}

	// Breaking down the expression
	expression.forEach(element => {

		// It's not a operator
		if ("+-/*".indexOf(element) == -1) {
			postFix.push(element);
		} else // It's a operator
		{
			// Checking the precedence and pusing operator into the stack
			while (stack.length != 0 && precedence[stack[stack.length - 1]] > precedence[element]) {
				postFix.push(stack.pop());
			}

			stack.push(element);
		}

	});
	// Adding the remaining operators from the stack
	while (stack.length > 0)
		postFix.push(stack.pop());

	return postFix;
}
var evaluatePostFix = function(postFix) {

	//console.log(line);
	var stack = Array();
	postFix.forEach(elements => {
		if ("+-/*".indexOf(elements) == -1) {
			stack.push(parseFloat(elements));
		} else {
			if (stack.length < 2)
				return null;
			var a = stack.pop(); //2nd Operand

			var b = stack.pop(); //1st Operand        
			stack.push(performOperation(b, a, elements));
		}
	});
	// Displaying the result on the webpage.
	return stack.pop();
}
var performOperation = function(a, b, operator) {
	switch (operator) {
		case "*":
			return parseFloat(a) * parseFloat(b);
		case "-":
			return parseFloat(a) - parseFloat(b);
		case "/":
			return parseFloat(a) / parseFloat(b);
		case "+":
			return parseFloat(a) + parseFloat(b);
		default:
			alert("Invalid Operation.");
			return null;
	}
}