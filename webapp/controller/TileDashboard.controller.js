sap.ui.define([
	"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel",
			"sap/ui/core/BusyIndicator",
				"sap/m/MessageToast"
], function(Controller,JSONModel,BusyIndicator,MessageToast) {
	"use strict";
	var oController,oView,oComponent;

	return Controller.extend("com.vSimpleApp.controller.TileDashboard", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.vSimpleApp.view.view.TileDashboard
		 */
			onInit: function() {
				oController = this;
			oView = this.getView();
			oComponent = this.getOwnerComponent();
		var jsonData = new sap.ui.model.json.JSONModel();
			jsonData.loadData("data.json/dataa.json");
			this.getView().setModel(jsonData, "jsonData");


	var oModel = this.getOwnerComponent().getModel("VHeader");
			
			oView = this.getView();
				var manifests = new sap.ui.model.json.JSONModel();
			manifests.loadData("model/cardManifests.json");
			this.getView().setModel(manifests, "manifests");
			console.log(manifests);
			
			
				var DemoProduccts = new sap.ui.model.json.JSONModel();
			DemoProduccts.loadData("model/DemoProducts.json");
			this.getView().setModel(DemoProduccts, "products");
			console.log(DemoProduccts);
			
			this.getVendorList();
			this.getPurchaseOrderList();
		this.getVendorList1();
	
			},
			handleLink1Press1:function(oEvent) {
						oComponent.getRouter().navTo("VendorCreate");
			}
,	handleLink1Press2:function(oEvent) {
						oComponent.getRouter().navTo("DisplayVendor");
			},	handleLink1Press3:function(oEvent) {
						oComponent.getRouter().navTo("EditVendor");
			},	handleLink1Press4:function(oEvent) {
						oComponent.getRouter().navTo("PurchaseOrderTable");
			},	handleLink1Press5        :function(oEvent) {
						oComponent.getRouter().navTo("EditPOOrder");
			},	handleLink1Pres7:function(oEvent) {
						oComponent.getRouter().navTo("EditPOOrder");
			},	handleLink1Press6:function(oEvent) {
						oComponent.getRouter().navTo("GoodReceipt");
			},
			
				navvbtt:function(oEvent) {
						oComponent.getRouter().navTo("Dashboard2");
			},
				pressGenericTile: function (evt) {
			//navigate the property is selected subheader.
					if (evt.getSource().getProperty("header") === "Vendor Master") {
							oComponent.getRouter().navTo("VendorCreate");
						} else if (evt.getSource().getProperty("header") === "Purchase Order") {
							oComponent.getRouter().navTo("PurchaseOrderTable");
						} else if (evt.getSource().getProperty("header") === "Post Goods Receipt") {
							oComponent.getRouter().navTo("GoodReceipt");
						} else if (evt.getSource().getProperty("header") === "Book Vendor Invoice") {
							oComponent.getRouter().navTo("Dashboard");
						} else if(evt.getSource().getProperty("header") === "Vendor Rebate Management"){
								oComponent.getRouter().navTo("DashboardVendor");
						}	
					

		},
			
					getVendorList: function() {
				var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//BusyIndicator.show(0);
			oModel.read("/Fetch_Vendor_DetailsSet", {
				success: function(oData) {
					var item = oData.results.length;
					var ListofVendor = [];

					for (var iRowIndex = 1300; iRowIndex <=1399; iRowIndex++) {
						var odata = oData.results[iRowIndex];

						
							var Lifnrr = odata.Lifnr;
							var Name1r = odata.Name1;
							ListofVendor.push({
								Lifnr: Lifnrr,
									Name1 : Name1r

							});
						
					}
					console.log(ListofVendor);
					
				
				
				
						var Count = new sap.ui.model.json.JSONModel({
							item: item

						});
						oView.setModel(Count, "Count");
					
					//BusyIndicator.hide();
					var oLookupModel = that.getOwnerComponent().getModel("Lookup");
					oLookupModel.setProperty("/DisplyaVendorList", ListofVendor);
					oLookupModel.refresh(true);
					//that.getMaterialList();
				},
				error: function(oError) {
					//BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
					},
						getPurchaseOrderList: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//	BusyIndicator.show(0);
			oModel.read("/POListSet", {
				success: function(oData) {
					
						BusyIndicator.hide();
						var itemPO = oData.results.length;
					var ListofPurchaseOrder = [];

					for (var iRowIndex = 1300; iRowIndex <=1399; iRowIndex++) {
						var odataset = oData.results[iRowIndex];
					
						
							var Compcode = odataset.Bukrs;
							var polost = odataset.Ebeln;
							var pogrp = odataset.Ekgrp;
							var poorg = odataset.Ekorg;
							var lifnrr = odataset.Lifnr;
							var currency = odataset.Waers;
				
						
							ListofPurchaseOrder.push({
								Bukrs: Compcode,
								Ebeln : polost,
								Ekgrp: pogrp,
								Ekorg : poorg,
								Lifnr: lifnrr,
								Waers : currency

							});
							
						
					}
					console.log(ListofPurchaseOrder);

						var CountPo = new sap.ui.model.json.JSONModel({
							item: itemPO

						});
						oView.setModel(CountPo, "CountPo");
					
					
				//	console.log(oData);
			
					var oLookupModel = that.getOwnerComponent().getModel("Lookup");
					oLookupModel.setProperty("/POOrderList", ListofPurchaseOrder);
					oLookupModel.refresh(true);
					//that.getMaterialList();
				},
				error: function(oError) {
					BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},
			
			
					getVendorList1: function() {
			var that = this;
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//BusyIndicator.show(0);
			oModel.read("/Fetch_Vendor_DetailsSet", {
				success: function(oData) {
					var item = oData.results.length;
					var ListofVendor = [];

					for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
						var odata = oData.results[iRowIndex];

						
							var Lifnrr = odata.Lifnr;
							var Name1r = odata.Name1;
							ListofVendor.push({
								Lifnr: Lifnrr,
									Name1 : Name1r

							});
						
					}
					console.log(ListofVendor);

					var ListofVendorData = new JSONModel();
					ListofVendorData.setData(ListofVendor);
					oView.setModel(ListofVendorData);

					sap.ui.getCore().setModel(ListofVendorData, "ListofVendorData");	
				//	console.log(oData);
				},
				error: function(oError) {
					//BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		},
			getPurchaseList1: function() {
		
			var oModel = this.getOwnerComponent().getModel("VHeader");
			//	BusyIndicator.show(0);
			oModel.read("/POListSet", {
				success: function(oData) {
				//	console.log(oData);
					BusyIndicator.hide();
						var item = oData.results.length;
					var ListofPurchaseOrder = [];

					for (var iRowIndex = 0; iRowIndex < item; iRowIndex++) {
						var odataset = oData.results[iRowIndex];
							var Compcode = odataset.Bukrs;
							var polost = odataset.Ebeln;
							var pogrp = odataset.Ekgrp;
							var poorg = odataset.Ekorg;
							var lifnrr = odataset.Lifnr;
							var currency = odataset.Waers;
				
						
							ListofPurchaseOrder.push({
								Bukrs: Compcode,
								Ebeln : polost,
								Ekgrp: pogrp,
								Ekorg : poorg,
								Lifnr: lifnrr,
								Waers : currency

							});
						
					}
					console.log(ListofPurchaseOrder);

					var ListofPOData = new JSONModel();
					ListofPOData.setData(ListofPurchaseOrder);
					oView.setModel(ListofPOData);

					sap.ui.getCore().setModel(ListofPOData, "ListofPOData");
				
				},
				error: function(oError) {
					BusyIndicator.hide();
					var errorMsg = oError.statusCode + " " + oError.statusText + ":" + JSON.parse(oError.responseText).error.message.value;
					MessageToast.show(errorMsg);
				}
			});
		}


		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.vSimpleApp.view.view.TileDashboard
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.vSimpleApp.view.view.TileDashboard
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.vSimpleApp.view.view.TileDashboard
		 */
		//	onExit: function() {
		//
		//	}

	});

});