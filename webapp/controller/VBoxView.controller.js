sap.ui.define([
	"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";
 var oController,oView,oComponent;
	return Controller.extend("com.vSimpleApp.controller.VBoxView", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.vSimpleApp.view.view.VBoxView
		 */
		onInit: function() {
				oController = this;
			oView = this.getView();
			oComponent = this.getOwnerComponent();
		var jsonData = new sap.ui.model.json.JSONModel();
			jsonData.loadData("data.json/dataa.json");
			this.getView().setModel(jsonData, "jsonData");

	
			},
			handleLink1Press1:function(oEvent) {
						oComponent.getRouter().navTo("VendorCreate");
			}
,	handleLink1Press2:function(oEvent) {
						oComponent.getRouter().navTo("DisplayVendor");
			},	handleLink1Press3:function(oEvent) {
						oComponent.getRouter().navTo("EditVendor");
			},	handleLink1Press4:function(oEvent) {
						oComponent.getRouter().navTo("PurchaseOrderTable");
			},	handleLink1Press5        :function(oEvent) {
						oComponent.getRouter().navTo("EditPOOrder");
			},	handleLink1Pres7:function(oEvent) {
						oComponent.getRouter().navTo("EditPOOrder");
			},	handleLink1Press6:function(oEvent) {
						oComponent.getRouter().navTo("GoodReceipt");
			}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.vSimpleApp.view.view.VBoxView
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.vSimpleApp.view.view.VBoxView
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.vSimpleApp.view.view.VBoxView
		 */
		//	onExit: function() {
		//
		//	}

	});

});