sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/m/Button",
	"sap/m/List",

	"sap/m/Text",
	"sap/m/library",
	"sap/m/MessageToast",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"com/vSimpleApp/model/CreateVendor"
], function(Controller, JSONModel, Filter, Button, List, Test, library, MessageToast, FilterOperator, MessageBox, Fragment, CreateVendor) {
	"use strict";
	var oView;
	return Controller.extend("com.vSimpleApp.controller.UpdateVendor", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.vSimpleApp.view.view.UpdateVendor
		 */
		onInit: function() {

			var oModel = this.getOwnerComponent().getModel("VHeader");
			//set the model on view to be used by the UI controls
			this.getView().setModel(oModel);
			console.log(oModel);

			// Define the models
			var UpdateContract = new CreateVendor();
			this.getView().setModel(UpdateContract.getModel(), "VendorContract");
			
			/*	var oEditModel = new JSONModel({
				isEditable: false
			});

			this.getView().setModel(oEditModel, "EditModel");

			var oEditContModel = new JSONModel({
				VendorNumber: "",
				CompCode: "",
				AccountGrp: "",
				PurchaseOrg: "",
				StreetHouse: "",
				PaymentTerm: "",
				FirstName: "",
				Email: "",
				Tcode: "",
				Telephone: "",
				LastName: "",
				PostalCode: "",
				City: "",
				OrderCurrency: "",
				Country: "",
				Region: ""
			});

			this.getView().setModel(oEditContModel, "AddEditModel");
			
*/
		},

		onSaveContractnew: function() {
			//define and bind the model
			//	var oVendorModel = oView.getModel("VendorModel");
			var getPurchase = this.getView().getModel("VendorContract");

			//	perSale.getData().Rate = oView.byId("psMeasure").getValue();

			//get all the values
			var VendorNumber = this.getView.byId("vnumber").getValue();
			//		getPurchase.getData().VendorNumber = oView.byId("vnumber").getValue();
			getPurchase.getData().AccountGrp = oView.byId("agro").getValue();
			getPurchase.getData().CompCode = oView.byId("ccode").getValue();
			getPurchase.getData().PurchaseOrg = oView.byId("porg").getValue();
			//	getPurchase.getData().VendorTitle = oView.byId("vtitle").getValue();         
			getPurchase.getData().FirstName = oView.byId("fname").getValue();
			getPurchase.getData().LastName = oView.byId("lname").getValue();
			getPurchase.getData().OrderCurrency = oView.byId("OrderCur").getValue();
			getPurchase.getData().StreetHouse = oView.byId("street").getValue();
			getPurchase.getData().Email = oView.byId("email").getValue();
			getPurchase.getData().PostalCode = oView.byId("pcode").getValue();
			getPurchase.getData().Country = oView.byId("vnumber").getValue();
			getPurchase.getData().PaymentTerm = oView.byId("idPayment").getValue();
			getPurchase.getData().Telephone = oView.byId("idTel").getValue();
			getPurchase.getData().City = oView.byId("city").getValue();
			getPurchase.getData().Region = oView.byId("idRegion").getValue();

			//		var LIFNR = oView.byId("vnumber").getValue();
			//var BUKRS = oView.byId("ccode").getValue();
			//	var EKORG = oView.byId("porg").getValue();
			//	var KTOKK = oView.byId("agro").getValue();
			//		var sVendorTitle = oView.byId("vtitle").getSelectedKey(); // get selected item's key

			//	var vTitle = oView.byId("vtitle").getValue();
			//	var NAME2 = oView.byId("fname").getValue();
			//	var NAME3 = oView.byId("lname").getValue();
			//	var STRAS = oView.byId("street").getValue();
			//	var PSTLZ = oView.byId("pcode").getValue();
			//	var ORT01 = oView.byId("city").getValue();
			//	var REGIO = oView.byId("country").getValue();
			//	var REGIO = oView.byId("rgn").getValue();
			//	var sTelPhone = oView.byId("tel").getValue();
			//	var sCode = oView.byId("cd").getValue();
			//	var sEmail = oView.byId("email").getValue();
			//	var sOrderCurrncy = oView.byId("OrderCur").getValue();
			//	var sPayment = oView.byId("payt").getValue();
			//		var LAND1 = oView.byId("concd").getValue();

			//Set all the value to model

			//	oVendorModel

			//define the service and get the model
			var oModelCreate = this.getView().getModel("VHeader");
			var that = this;

			//define the array/
			var oEntry1 = {};
			oEntry1.Lifnr = VendorNumber;
			//	oEntry1.Bukrs = getPurchase.getData().AccountGrp;
			oEntry1.Bukrs = getPurchase.getData().CompCode;
			oEntry1.Ekorg = getPurchase.getData().PurchaseOrg;
			oEntry1.Name2 = getPurchase.getData().FirstName;
			oEntry1.Name3 = getPurchase.getData().LastName;
			//	oEntry1.OrderCurrency = getPurchase.getData().OrderCurrency;
			oEntry1.Stras = getPurchase.getData().StreetHouse;
			//		oEntry1.Email = getPurchase.getData().Email;
			//		oEntry1.PostalCode = getPurchase.getData().PostalCode;
			//	oEntry1.Country = getPurchase.getData().Country;
			//	oEntry1.PaymentTerm = getPurchase.getData().PaymentTerm;
			//	oEntry1.Telephone = getPurchase.getData().Telephone;
			oEntry1.Ort01 = getPurchase.getData().City;
			oEntry1.Regio = getPurchase.getData().Region;
			//		oEntry1.Tcode = getPurchase.getData().Tcode;

			//bind the values to array
			/*	oEntry1.Land1 = LAND1;
				oEntry1.Lifnr = LIFNR;
				oEntry1.Bukrs = BUKRS;
				oEntry1.Ekorg = EKORG;
				oEntry1.ktokk = KTOKK;
				oEntry1.Name2 = NAME2;
				oEntry1.Name3 = NAME3;
				oEntry1.Stras = STRAS;
				oEntry1.Pstlz = PSTLZ;
				oEntry1.Ort01 = ORT01;
				oEntry1.Regio = REGIO;*/
			//	oEntry1.telp = sTelPhone;
			//	oEntry1.cdd = sCode;
			//	oEntry1.Email = sEmail;
			///	oEntry1.currncy = sOrderCurrncy;
			//	oEntry1.Paymt = sPayment;
			//	oEntry1.VendorTitle= sVendorTitle;

			oModelCreate.update("/UpdateEntitySet('" + VendorNumber + "')", oEntry1, {
				/*		success : function(odata,oResponse){
					if(odata ! == "" || odata ! == undefined) {
						MessageBox.success("Update Succesfully");
					},
					{
						MessageBox.error("Not Updated");
					}
				}
	*/
			});

			/*	oModelCreate.create('/Vendor_CreateSet', oEntry1, {

					//	success: MessageToast.show("Inserted Successfully"), //that._onCreateEntrySuccess.bind(that),
					success: this._onCreateEntrySuccess.bind(this),
					error: that._onCreateEntryError.bind(that)
				}

			);
*/
		},

		onDisplayPress: function() {
			var oComponent1 = this.getOwnerComponent();
			oComponent1.getRouter().navTo("DisplayVendor");
		},
		onCreatePress: function() {
			var oComponent = this.getOwnerComponent();
			oComponent.getRouter().navTo("CreateContractVendor");
		},
			OnCancelContract: function() {
								//cancel model and reset all the values
						//		MessageToast.show("Cancel Contract");
								oView.byId("idVendor").setValue("");
								oView.byId("idCompCode").setValue("");
								oView.byId("idPurOrg").setValue("");
								oView.byId("idAccGp").setValue("");
								oView.byId("idCountryCode").setValue("");
								oView.byId("idFname").setValue("");

								oView.byId("idLname").setValue("");
								oView.byId("idStreet").setValue("");

								oView.byId("idPostcode").setValue("");
								oView.byId("idCity").setValue("");
								oView.byId("idRegion").setValue("");
								oView.byId("idTcode").setValue("");
								oView.byId("idTel").setValue("");
								//		oView.byId("cd").setValue("");
								oView.byId("idEmail").setValue("");
								oView.byId("idOrderCur").setValue("");
								oView.byId("idPayment").setValue("");

								//redirect the page	frot view
								var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
								oRouter.navTo("ShowTiles");

							}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.vSimpleApp.view.view.UpdateVendor
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.vSimpleApp.view.view.UpdateVendor
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.vSimpleApp.view.view.UpdateVendor
		 */
		//	onExit: function() {
		//
		//	}

	});

});