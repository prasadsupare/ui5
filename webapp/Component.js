sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/vSimpleApp/model/models",
	"sap/ui/model/json/JSONModel",
	"com/vSimpleApp/service/Application",
	"sap/ui/core/IconPool",
	"com/vSimpleApp/model/Contract",
	"com/vSimpleApp/model/Report",
	"com/vSimpleApp/model/GetPurchaseVendor",
		"com/vSimpleApp/model/CreateVendor"
	/*	"com/vSimpleApp/model/POrdersList"*/
], function(UIComponent, Device, models,JSONModel,Application,IconPool,Contract,Report,GetPurchaseVendor,CreateVendor) {
	"use strict"; 
	var oComponent;

	return UIComponent.extend("com.vSimpleApp.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
		
				var oUserData = {
				Username: ""
			};
			var oUserModel = new JSONModel(oUserData);
					this.setModel(oUserModel, "User");
					
					
					
			var oLookupData = {
				SelectedTabKey: "item",
				IsContractItemSaved: false,
				IsContractAccrualSaved: false,
				IsContractSettlementSaved: false,
				VendorList: [],
				DisplyaVendorList: [],
				POVendorList: [],
				POOrderList: [],
				MaterialList: [],
				POPlant: [],
				PurchaseOrganization : [],
				CountryCode: [],
				AccountGroup:[],
				CountryCodeRegion: [],
				StorageLocationList:[],
				MaterialDiscription:[],
				PurchaseGroupList:[]
			};
			var oLookupModel = new JSONModel(oLookupData);
			this.setModel(oLookupModel, "Lookup");
			
			
			
			
				var oPurchaseData = {
				TempContract: new GetPurchaseVendor(),
			//	ContractList: new POrdersList()
			};
			var oPurchaseModel = new JSONModel(oPurchaseData);
			this.setModel(oPurchaseModel, "PurchaseModel");
			
			
			
			
			
			var CreateVendorData = {
				VendorContract: new CreateVendor()
			};
			var CreateVendorModel = new JSONModel(CreateVendorData);
			this.setModel(CreateVendorModel, "CreateVendor");
		
			var oVendorData = {
				TempContract: new Contract(),
				ContractList: []
			};
			var oVendorModel = new JSONModel(oVendorData);
			this.setModel(oVendorModel, "Vendor");
			this.loadIconLibraries();
			Application.getInstance().Component = this;
			
			
			
			//getCountrymodel defined here
			
				var oGetCountryModel = new JSONModel();
    		oGetCountryModel.setData([]);
    		this.setModel(oGetCountryModel, "GetCountryModel");
    
			
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
				// create the views based on the url/hash
			this.getRouter().initialize();
			
			
			/*Started sim service*/
			
					this.setModel(Report.getInstance().getModel(),"report");
			
			var postData = [
				{
					postId: "1",
					status: 1,
					workflow: 1,
					vendorNo: 'V23425',
					vendorName: 'ABC Corporation',
					poNo: 'P123',
					grossAmount: 990,
					currencyCode: 'USD',
					currencySymbol: '$',
					docId: "D123455",
					invoiceNo: "IN12345",
					process: "MM",
					docDate: "2018-07-19"
				},
				{
					postId: "2",
					status: 2,
					workflow: 2,
					vendorNo: 'V56545',
					vendorName: 'XYZ Inc.',
					poNo: 'P234',
					grossAmount: 123,
					currencyCode: 'USD',
					currencySymbol: '$',
					docId: "D232323",
					invoiceNo: "IN23232",
					process: "FI",
					docDate: "2018-07-19"
				},
				{
					postId: "3",
					status: 0,
					workflow: 0,
					vendorNo: 'V67686',
					vendorName: 'Alpha Pvt. Ltd.',
					poNo: 'P345',
					grossAmount: 145,
					currencyCode: 'USD',
					currencySymbol: '$',
					docId: "D343345",
					invoiceNo: "IN34332",
					process: "FI",
					docDate: "2018-07-19"
				}
			];
			
			var oModel = new sap.ui.model.json.JSONModel({
				PostData: postData
			});
			
			this.setModel(oModel);
			
			var vendorNoData = [
				{
					vendorNo: "VL00451100",
					vendorName: "Acer Laptop",
					pincode: "67453"
				},
				{
					vendorNo: "VL00453300",
					vendorName: "3M",
					pincode: "55144"
				},
				{
					vendorNo: "VL00452200",
					vendorName: "Lenovo",
					pincode: "67330"
				}
			];
			
			var vendorNoDataModel = new sap.ui.model.json.JSONModel(vendorNoData);
			this.setModel(vendorNoDataModel, "VendorNoData");
			
		    var nonSapErrorDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(nonSapErrorDataModel, "NonSapErrorData");
		    
			var sapErrorDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(sapErrorDataModel, "SapErrorData");
			
			var mgrApprovalDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(mgrApprovalDataModel, "MgrApprovalData");
			
			var warningDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(warningDataModel, "WarningData");
			
			var inProcessDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(inProcessDataModel, "InProcessData");
			
			var rejectedDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(rejectedDataModel, "RejectedData");
			
			var chartDataModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(chartDataModel, "chartData");
			
			var manualVerifyDocsModel = new sap.ui.model.json.JSONModel([]);
			this.setModel(manualVerifyDocsModel, "manualVerifyDocuments");
			
			
			
			/*ended sim service*/			
			
	
		},
		
		/*Vendor rebate start*/
			loadIconLibraries: function() {
			var b = [];
			var c = {};
			//Fiori Theme font family and URI
			var t = {
				fontFamily: "SAP-icons-TNT",
				fontURI: sap.ui.require.toUrl("sap/tnt/themes/base/fonts/")
			};
			//Registering to the icon pool
			IconPool.registerFont(t);
			b.push(IconPool.fontLoaded("SAP-icons-TNT"));
			c["SAP-icons-TNT"] = t;
			//SAP Business Suite Theme font family and URI
			var B = {
				fontFamily: "BusinessSuiteInAppSymbols",
				fontURI: sap.ui.require.toUrl("sap/ushell/themes/base/fonts/")
			};
			//Registering to the icon pool
			IconPool.registerFont(B);
			b.push(IconPool.fontLoaded("BusinessSuiteInAppSymbols"));
			c["BusinessSuiteInAppSymbols"] = B;
			
			/*Vendor rebate end*/
		}
		
		
		
		
	
	});
});